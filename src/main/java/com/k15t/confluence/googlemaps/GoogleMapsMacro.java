package com.k15t.confluence.googlemaps;


import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.renderer.RenderContext;
import org.apache.commons.lang.StringUtils;

import java.lang.Override;
import java.lang.String;
import java.util.Map;


public class GoogleMapsMacro implements Macro {

    private static final String template = "velocity/googlemaps.vm";
    private static final String template_PDF = "velocity/googlemaps-static.vm";

    private static final String PARAM_LATITUDE = "latitude";
    private static final String PARAM_LONGITUDE = "longitude";

    @Override
    public String execute(Map<String, String> properties, String body, ConversionContext conversionContext)
            throws MacroExecutionException {

        String latitude = properties.get(PARAM_LATITUDE);
        String longitude = properties.get(PARAM_LONGITUDE);

        if (StringUtils.isEmpty(latitude) || StringUtils.isEmpty(longitude)) {
            throw new MacroExecutionException("Required parameter is missing!");
        }

        Map velocityContext = MacroUtils.defaultVelocityContext();
        velocityContext.put(PARAM_LATITUDE, latitude);
        velocityContext.put(PARAM_LONGITUDE, longitude);

        if (RenderContext.PDF.equals(conversionContext.getPageContext().getOutputType())) {
            return VelocityUtils.getRenderedTemplate(template_PDF, velocityContext);
        } else {
            return VelocityUtils.getRenderedTemplate(template, velocityContext);
        }
    }


    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }


    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }
}

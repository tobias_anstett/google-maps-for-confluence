(function ($) {

    AJS.bind('init.rte', function () {

        var DEFAULT_LATLNG = new google.maps.LatLng(40.713956, -74.006653);

        var dialog;

        var map;
        var marker;

        AJS.MacroBrowser.setMacroJsOverride('google-maps-macro', {
            opener:function (macro) {
                openCoordinatesDialog(macro);
            }
        });

        function openCoordinatesDialog(macro) {

            if (!dialog) {
                dialog = new AJS.Dialog({
                    width: 1100,
                    height: 550,
                    id: "manage-coordinates-dialog"
                });
                dialog.addHeader("Manage Coordinates");
                dialog.addPanel("ManageCoordinatesPanel", "manageCoordinatesPanel1");
                dialog.getCurrentPanel().html(Confluence.ManageCoordinates.Templates.manageCoordinatesForm());
                dialog.addSubmit("Save", insertMacroIntoEditor);
                dialog.addCancel("Cancel", cancelChanges);
            }

            var latitude;
            var longitude;

            if(typeof macro.params != 'undefined') {
                latitude = macro.params.latitude;
                longitude = macro.params.longitude;
            }

            setFieldValues(latitude, longitude);
            createMap(latitude, longitude);
            createMarker(latitude, longitude);
            createListeners();

            dialog.show();
        }

        function setFieldValues(latitude, longitude) {
            if(typeof latitude == 'undefined') {
                latitude = "";
            }
            if(typeof longitude == 'undefined') {
                longitude = "";
            }
            document.getElementById("latitude-value").value = latitude;
            document.getElementById("longitude-value").value = longitude;
        }


        function createMap(latitude, longitude) {
            var latLng;
            if (typeof  latitude != "undefined" && typeof  longitude != "undefined") {
                latLng = new google.maps.LatLng(latitude, longitude);
            } else {
                latLng = DEFAULT_LATLNG;
            }

            var mapOptions = {
                zoom: 8,
                center: latLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            map = new google.maps.Map(document.getElementById("manage-coordinates-canvas"), mapOptions);
        }

        function createMarker(latitude, longitude) {

            if (typeof  latitude != "undefined" && typeof  longitude != "undefined") {
                marker = new google.maps.Marker({
                    draggable: true,
                    position: new google.maps.LatLng(latitude, longitude),
                    map: map
                });

            } else {
                marker = new google.maps.Marker({
                    draggable: true,
                    position: DEFAULT_LATLNG,
                    map: map,
                    visible: false
                });
            }
        }

        function createListeners() {
            google.maps.event.addListener(map, 'click', function (overlay, point) {
                marker.setPosition(overlay.latLng);
                marker.visible = true;
                document.getElementById("latitude-value").value = overlay.latLng.lat();
                document.getElementById("longitude-value").value = overlay.latLng.lng();
            });
            google.maps.event.addListener(marker, "dragend", function(event) {
                document.getElementById("latitude-value").value = event.latLng.lat();
                document.getElementById("longitude-value").value = event.latLng.lng();
            });
        }


        function insertMacroIntoEditor () {
            $("#latitude-error").hide();
            $("#longitude-error").hide();
            $("#type-error").hide();

            var valid = true;

            if(document.getElementById("latitude-value").value == "") {
                $("#latitude-error").html("Please set this value by clicking in the map.");
                $("#latitude-error").show();
                valid = false;
            }
            if(document.getElementById("longitude-value").value == "") {
                $("#longitude-error").html("Please set this value by clicking in the map.");
                $("#longitude-error").show();
                valid = false;
            }

            if(valid) {
                var macroRenderRequest = {
                    contentId:Confluence.Editor.getContentId(),
                    macro:getMacroDefinition()
                };
                tinymce.confluence.MacroUtils.insertMacro(macroRenderRequest);
                dialog.hide();
            }
        };


        function getMacroDefinition() {
            return {
                name: "google-maps-macro",
                params: {
                    latitude: document.getElementById("latitude-value").value,
                    longitude: document.getElementById("longitude-value").value
                }
            };
        };


        function cancelChanges() {
            dialog.hide();
        }

    });

})(AJS.$);

$(document).ready(function () {

    var div = $('#google-maps-placeholder')[0];

    var latitude = AJS.$(div).attr('data-coordinates-latitude');
    var longitude = AJS.$(div).attr('data-coordinates-longitude');

    var mapOptions = {
        center: new google.maps.LatLng(latitude, longitude),
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    var map = new google.maps.Map(div, mapOptions);

    var coordinates = new google.maps.LatLng(latitude, longitude);

    var marker = new google.maps.Marker({
        position: coordinates,
        map: map,
        animation: google.maps.Animation.DROP
    });
});